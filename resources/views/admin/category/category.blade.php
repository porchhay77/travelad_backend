@extends('admin.layouts.app')
@include('admin.category.active')
@section('main-content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><b>Create Category</b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Category</a></li>
              <li class="breadcrumb-item active">Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default card -->
      <div class="card">
        <div class="card-header with-border">


            <a class='col-lg-offset-5 btn btn-success' href="{{ route('category.index') }}">Back</a>

          <div class=" pull-right card-tools">
            <button type="button" class="btn btn-card-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-card-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <form role="form" action="{!! route('category.store') !!}" method="post">
              @csrf

                @include('includes.messages')

                  <div class="form-group">
                    <label for="exampleInputEmail1">Category Title</label>
                    <input type="text" class="form-control" id="title" name="name" placeholder="Category title">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1"> Slug</label>
                    <input type="text" class="form-control" id="slug" name="slug" placeholder="Category slug">
                  </div>

              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

          <!-- /.card -->
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>


@endsection
