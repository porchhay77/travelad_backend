<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    @include('admin.layouts.head')
  </head>
  <body class="hold-transition sidebar-mini">
    <body><div class="wrapper">
      @include('admin.layouts.header')
      @include('admin.layouts.sidebar')
      @section('main-content')
        @show

      @include('admin.layouts.footer')
    </div>

  </body>
</html>
