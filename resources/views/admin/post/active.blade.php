@section('blogSection')
  <li class="nav-item has-treeview menu-open">
    <a href="#" class="nav-link active">
      <i class="nav-icon fas fa-blog"></i>
      <p>
        Blog
        <i class="right fas fa-angle-left"></i>
      </p>
    </a>
    <ul class="nav nav-treeview">
      <li class="nav-item">
        <a href="{!! route('post.index') !!}" class="nav-link active">
          <i class="far fa-circle nav-icon"></i>
          <p>Posts</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{!! route('category.index') !!}" class="nav-link ">
          <i class="far fa-circle nav-icon"></i>
          <p>Categories</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{!! route('tag.index') !!}" class="nav-link">
          <i class="far fa-circle nav-icon"></i>
          <p>Tags</p>
        </a>
      </li>
    </ul>
  </li>
@endsection
