@extends('admin.layouts.app')
@include('admin.post.active')
@section('main-content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><b>Edit Post</b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Post</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="card">
        <div class="card-header with-border">

            <a class='btn btn-success' href="{{ route('post.index') }}">Back</a>
          <div class="pull-right card-tools">
            <button type="button" class="btn btn-card-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-card-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">

            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{!! route('post.update',$post->id) !!}" method="post">
              @csrf
              {{method_field('PUT')}}
                @include('includes.messages')
                  <div class="form-group">
                    <label for="exampleInputEmail1">Post Title</label>
                    <input type="text" value="{{$post->title}}"class="form-control" id="title" name="title" placeholder="Title">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Post Sub Title</label>
                    <input type="text" value="{{$post->subtitle}}" class="form-control" id="subtitle" name="subtitle" placeholder="Sub Title">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Post Slug</label>
                    <input type="text" value="{{$post->slug}}" class="form-control" id="slug" name="slug" placeholder="Slug">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Image input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="image" value="{{$post->image}}" id="image" class="custom-file-input">
                        <label class="custom-file-label" for="image">Image</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-check">
                    <input type="checkbox" name="status" @if($post->status == NULL)checked @endif class="" id="status">
                    <label class="form-check-label" value="{{$post->status}}" for="exampleCheck1">Status</label>
                  </div><br/>
                  <div class="forn-textarea">
                    <div class="mb-3">
                      <textarea id="editor1" name="body" style="width: 100%">{{$post->body}}</textarea>
                    </div>
                  </div>
              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>


@endsection
@section('footerSection')
  <script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>
  <!-- Bootstrap 4 -->

  <!-- CK Editor -->
  <script src="{{asset('admin/ckeditor/ckeditor.js')}}"></script>

  <!-- Bootstrap WYSIHTML5 -->

  <script>
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
      });
  </script>

@endsection
