@extends('admin.layouts.app')

@section('headSection')
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection

@include('admin.post.active')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><b>Post List</b></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Post</a></li>
            <li class="breadcrumb-item active"></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default card -->
    <div class="card">
      <div class="card-header border-0">


          <a class='col-lg-offset-5 btn btn-success' href="{{ route('post.create') }}">Add New</a>

        <div class=" pull-right card-tools">
          <button type="button" class="btn btn-card-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-card-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">
                    <!-- /.card-header -->

                      <table id="example1" class="table table-bordered table-striped">
                        <thead>
                           <tr>
                             <th>S.No</th>
                             <th>Title</th>
                             <th>Sub Title</th>
                             <th>Slug</th>
                             <th>Creatd At</th>
                             <th>Edit</th>
                             <th>Delete</th>

                           </tr>
                         </thead>
                         <tbody>
                         @foreach ($posts as $post)
                           <tr>
                             <td>{{ $loop->index + 1 }}</td>
                             <td>{{ $post->title }}</td>
                             <td>{{ $post->subtitle }}</td>
                             <td>{{ $post->slug }}</td>
                             <td>{{ $post->created_at }}</td>


                               <td><a href="{{ route('post.edit',$post->id) }}"><span class="fas fa-edit"></span> Edit</a> </td>



                             <td>
                               <form id="delete-form-{{ $post->id }}" method="post" action="{{ route('post.destroy',$post->id) }}" style="display: none">
                                 {{ csrf_field() }}
                                 {{ method_field('DELETE') }}
                               </form>
                               <a href="" onclick="
                               if(confirm('Are you sure, You Want to delete this?'))
                                   {
                                     event.preventDefault();
                                     document.getElementById('delete-form-{{ $post->id }}').submit();
                                   }
                                   else{
                                     event.preventDefault();
                                   }" ><span class="fas fa-trash"></span> Delete</a>
                             </td>

                           </tr>
                         @endforeach
                         </tbody>
                        <tfoot>

                        </tfoot>
                      </table>


      </div>

    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@section('footerSection')
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
  $(function () {
    $("#example2").DataTable();
  });
</script>
@endsection
