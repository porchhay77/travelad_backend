@extends('admin.layouts.app')
@include('admin.tag.active')
@section('main-content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><b>Add Tag</b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Tag</a></li>
              <li class="breadcrumb-item active">Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default card -->
      <div class="card">
        <div class="card-header with-border">


            <a class='col-lg-offset-5 btn btn-success' href="{{ route('tag.index') }}">Back</a>

          <div class=" pull-right card-tools">
            <button type="button" class="btn btn-card-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-card-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <form role="form" action="{!! route('tag.store') !!}" method="post">
                @csrf

                @include('includes.messages')
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tag Title</label>
                    <input type="text" class="form-control" id="title" name="name" placeholder="Tag title">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Tag Slug</label>
                    <input type="text" class="form-control" id="slug" name="slug" placeholder="Tag slug">
                  </div>

              </div>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>

          <!-- /.card -->
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>


@endsection
