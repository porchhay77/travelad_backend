<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','User\HomeController@index');
Route::get('post','User\PostController@index')->name('post');

Route::get('admin/home', function () {
    return view('admin.home');
});


Route::post('post',function(){
    return view('user/post');
})->name('post');


Route::resource('admin/post','Admin\PostController');
Route::resource('admin/tag','Admin\TagController');
Route::resource('admin/category','Admin\CategoryController');
